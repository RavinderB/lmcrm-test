<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="{{ asset('assets/web/js/script.js') }}"></script>
</head>
<body>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;display: inline-block;margin-right:10px;vertical-align: top;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-yzt1{background-color:#efefef;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
		.none{display:none;}
	</style>
	<table class="tg" id="info-table">
	  <tr>
	    <th class="tg-yzt1">Icon</th>
	    <th class="tg-yzt1">Date</th>
	    <th class="tg-yzt1">Name</th>
	    <th class="tg-yzt1">Phone</th>
	    <th class="tg-yzt1">Email</th>
	  </tr>
	  @foreach ($results as $result)
		  <tr data-id="{{ $result->data_id }}">
		    <td class="tg-yw4l td-icon"></td>
		    <td class="tg-yw4l td-date">{{ $result->date }}</td>
		    <td class="tg-yw4l td-name">{{ $result->name }}</td>
		    <td class="tg-yw4l td-phone">{{ $result->phone }}</td>
		    <td class="tg-yw4l td-email">{{ $result->email }}</td>
		  </tr>
	  @endforeach
	</table>
	<table class="tg none" style="table-layout: fixed; width: 63px" id="data-table">
	<colgroup>
	<col style="width: 21px">
	<col style="width: 21px">
	<col style="width: 21px">
	</colgroup>
	  <tr>
	    <th class="tg-yzt1">Icon</th>
	    <th class="tg-3we0 icon-td"></th>
	    <th class="tg-3we0" id="close-table">X</th>
	  </tr>
	  <tr>
	    <td class="tg-yzt1">Date</td>
	    <td class="tg-yw4l" colspan="2">DATE</td>
	  </tr>
	  <tr>
	    <td class="tg-yzt1">Name</td>
	    <td class="tg-yw4l" colspan="2">NAME</td>
	  </tr>
	  <tr>
	    <td class="tg-yzt1">Phone</td>
	    <td class="tg-yw4l" colspan="2">PHONE</td>
	  </tr>
	  <tr>
	    <td class="tg-yzt1">Email</td>
	    <td class="tg-yw4l" colspan="2">EMAIL</td>
	  </tr>
	</table>
</body>
</html>