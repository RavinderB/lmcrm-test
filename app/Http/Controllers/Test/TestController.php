<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Sentinel;

class TestController extends Controller
{
    public function index(Request $request)
    {
    	$array = [];
    	if($request->ajax()){
	        $results = DB::table('leads')->join('customers', 'leads.customer_id', '=', 'customers.id')
	    		->join('sphere_attributes', 'leads.sphere_id', '=', 'sphere_attributes.sphere_id')
	    		->join('sphere_attribute_options', 'sphere_attributes.id', '=', 'sphere_attribute_options.sphere_attr_id')
	            ->where('leads.id', 8)
	            ->where('sphere_attribute_options.ctype', 'agent')
	            ->select(
	            	'sphere_attribute_options.id as OID',
	            	'sphere_attributes.id as AID',
	            	'sphere_attributes.sphere_id as table_id')
	            ->get(); 
	        foreach ($results as $result) {
	        	$querys = DB::table('leads')->join('customers', 'leads.customer_id', '=', 'customers.id')
		    		->join('sphere_attributes', 'leads.sphere_id', '=', 'sphere_attributes.sphere_id')
		    		->join('sphere_attribute_options', 'sphere_attributes.id', '=', 'sphere_attribute_options.sphere_attr_id')
		    		->join('sphere_bitmask_'.$result->table_id, 'sphere_bitmask_'.$result->table_id.'.user_id', '=', 'leads.id')
		            ->where('leads.id', 8)
		            ->where('sphere_attribute_options.ctype', 'agent')
		            ->where('sphere_bitmask_'.$result->table_id.'.type', 'lead')
		            ->where('sphere_bitmask_'.$result->table_id.'.fb_'.$result->AID.'_'.$result->OID, 1)
		            ->select(
		            	'sphere_attributes.label as field',
		            	'sphere_attribute_options.value as value')
		            ->get();
		        foreach ($querys as $query) {
		        	$array[$query->field][] = $query->value;
		        }

	        }

	        echo json_encode($array);
        } else {
        	$results = DB::table('leads')->join('customers', 'leads.customer_id', '=', 'customers.id')
    		->join('open_leads', 'leads.id', '=', 'open_leads.lead_id')
            ->where('open_leads.agent_id', Sentinel::getUser()->id)
            ->select(
            	'leads.date as date', 
            	'leads.name as name',
            	'leads.email as email',
            	'customers.phone as phone',
            	'leads.id as data_id'
            )->get();
	        $data = ['results' => $results];
	        return view('test.index', $data);
        }
    }
}
