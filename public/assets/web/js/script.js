$(document).ready(function(){
	$("#info-table tr").not(":first-child").click(function(){
		getData($(this).data("id"), appendData ,$(this));
	});

	$("#close-table").click(function(){
		$("#data-table").addClass("none");
	});

	function appendData(result, elem){
		$(".dynamic").each(function(){
			$(this).remove();
		});
		$("#data-table").find("tr").not(":first-child").each(function(index){
			var data = new Array();
			var self = $(this);
			$(elem).find("td").not(":first-child").each(function(index){
				data.push($(this).text());
			});
			$(this).find("td:last-child").each(function(){
				$(this).text(data[index] || "EMPTY");
			});
		});
		for(el in result){
			$("#data-table").append('<tr class="dynamic"><td class="tg-yzt1">'+el+'</td><td class="tg-yw4l" colspan="2">'+result[el].join(",")+'</td></tr>');
		}
		$("#data-table").removeClass("none");
	}

	function getData(id, callback, elem){
		$.ajax({
			url:"/test_ex",
			data:"id="+id,
			type:"GET",
			success:function(result){
				callback(JSON.parse(result),elem);
			},
			error:function(xhr,status){
				alert(status);
			}
		});
	}
});